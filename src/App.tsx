import React, { useCallback, useEffect, useState } from "react";
import "./App.css";
import { Redirect, Route, Switch } from "react-router-dom";
import { FormConfiguration, FormConfigurationCollection, loadForms, saveForms } from "./designer/storage";
import { FormDesignerPage } from "./designer/FormDesignerPage";
import { FormPage } from "./viewer/FormPage";
import { FormListingPage } from "./designer/FormListingPage";

const App = () => {
    const [forms, setForms] = useState<FormConfigurationCollection>(loadForms());
    const saveForm = useCallback(
        (form: FormConfiguration) => setForms((prev) => ({ ...prev, [form.id]: form })),
        [setForms]
    );

    const deleteForm = useCallback(
        (id: string) =>
            setForms((prev) => {
                const updated = { ...prev };
                delete updated[id];
                return updated;
            }),
        [setForms]
    );

    useEffect(() => {
        saveForms(forms);
    }, [forms]);

    const renderFormDesignerPage = useCallback(
        () => <FormDesignerPage saveForm={saveForm} forms={forms} />,
        [forms, saveForm]
    );

    const renderFormListingPage = useCallback(() => <FormListingPage forms={forms} />, [forms]);

    return (
        <Switch>
            <Route
                path="/designer/:id/delete"
                render={(props) => {
                    deleteForm(props.match.params.id);
                    return <Redirect to="/" />;
                }}
            />
            <Route path="/designer/:id?" render={renderFormDesignerPage} />
            <Route path="/form" component={FormPage} />
            <Route path="/" render={renderFormListingPage} />
        </Switch>
    );
};

export default App;
