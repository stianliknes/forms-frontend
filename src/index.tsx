import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import {MithraProvider} from "@dossier/mithra-ui";
import "@dossier/mithra-ui/dist/ds.css"
import {BrowserRouter} from "react-router-dom";

ReactDOM.render(
    <React.StrictMode>
        <MithraProvider>
            <BrowserRouter>
                <App/>
            </BrowserRouter>
        </MithraProvider>
    </React.StrictMode>,
    document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
