import { ApplicationLayout, NavbarButton, NavbarSection } from "@dossier/mithra-ui";
import { NavLink } from "react-router-dom";
import React from "react";

interface Props extends Partial<React.ComponentProps<typeof ApplicationLayout>> {
    headerText: string;
}

export const Page: React.FC<Props> = ({
    headerText,
    pageType = "user",
    navbarSections = (
        <>
            <NavbarSection>
                <NavbarButton as={NavLink} exact icon="UserHome" text="My forms" to="/" />
            </NavbarSection>
        </>
    ),
    children,
}) => (
    <ApplicationLayout
        isMainPage={true}
        backButtonTitle="Back to forms"
        headerButtons={<></>}
        headerText={headerText}
        mobileIconButtons={[
            {
                icon: "Adjustments",
                onClick: () => {},
                title: "Settings",
            },
        ]}
        navbarSections={navbarSections}
        pageType={pageType}
        skipToMainContentText="gui.standard.skipToMainContent"
        tray={null}
    >
        <div
            style={{
                maxWidth: "74ch",
            }}
        >
            {children}
        </div>
    </ApplicationLayout>
);
