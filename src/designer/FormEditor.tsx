import { Button, FormLabel, IconButton, Input, Stack } from "@dossier/mithra-ui";
import React, { useCallback, useEffect, useState } from "react";
import { Field, FormConfiguration } from "./storage";
import { FieldEditor } from "./FieldEditor";
import { Link } from "react-router-dom";

export const FormEditor: React.FC<{
    form: FormConfiguration;
    saveForm: (config: FormConfiguration) => void;
}> = ({ form, saveForm }) => {
    const [fields, setFields] = useState<Field[]>(form.fields);
    const [formName, setFormName] = useState(form.name);

    const addField = useCallback(() => setFields((prev) => [...prev, { label: "", type: "text" }]), [setFields]);

    const updateField = useCallback(
        (fieldId: number, updates: Partial<Field>) =>
            setFields((prev) => prev.map((v, i) => (i === fieldId ? { ...v, ...updates } : v))),
        [setFields]
    );

    const removeField = useCallback(
        (fieldId: number) => setFields((prev) => prev.filter((v, i) => i !== fieldId)),
        [setFields]
    );

    const createLink = () =>
        `${window.location.origin}/form?name=${formName}&fields=${encodeURIComponent(JSON.stringify(fields))}`;

    useEffect(() => {
        if (fields !== form.fields || formName !== form.name) {
            saveForm({ id: form.id, name: formName, fields: fields });
        }
    }, [fields, formName, form, saveForm]);

    return (
        <Stack flexDirection="column" alignItems="flex-start">
            <div>
                <FormLabel label="Form name" />
                <Input type="text" value={formName} onChange={(event) => setFormName(event.target.value)} />
            </div>

            {fields.map((data, i) => (
                <Stack key={i} alignItems="flex-end">
                    <FieldEditor {...data} update={(data) => updateField(i, data)} />
                    <IconButton icon="Close" title="Remove" onClick={() => removeField(i)} variant="card-item-action" />
                </Stack>
            ))}

            <Stack alignItems="flex-start">
                <Button onClick={addField}>Add field</Button>
            </Stack>

            <Button as="a" href={createLink()} target="_blank">
                Open form
            </Button>

            <Button as={Link} to={`/designer/${form.id}/delete`}>
                Delete form
            </Button>
        </Stack>
    );
};
