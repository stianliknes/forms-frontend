import React, { useCallback } from "react";
import { Field } from "./storage";
import { FormLabel, Input, Select, Stack } from "@dossier/mithra-ui";

export const FieldEditor: React.FC<Field & { update: (data: Partial<Field>) => void }> = ({ label, update }) => {
    const updateFieldName = useCallback((event) => update({ label: event.target.value }), [update]);
    const updateFieldType = useCallback((event) => update({ type: event.target.value as Field["type"] }), [update]);

    return (
        <>
            <Stack>
                <div>
                    <FormLabel label="Field name" />
                    <Input value={label} onChange={updateFieldName} />
                </div>
                <div>
                    <FormLabel label="Type" />
                    <Select status={undefined} onChange={updateFieldType}>
                        <option value="text">Text</option>
                        <option value="email">Email address</option>
                    </Select>
                </div>
            </Stack>
        </>
    );
};
