import { FormConfiguration, FormConfigurationCollection } from "./storage";
import { Redirect, useParams } from "react-router-dom";
import { FormEditor } from "./FormEditor";
import React, {useState} from "react";
import { Page } from "../common/Page";

const generateFormId = () => Date.now().toString();

export const FormDesignerPage = ({
    saveForm,
    forms,
}: {
    saveForm: (form: FormConfiguration) => void;
    forms: FormConfigurationCollection;
}) => {
    const params = useParams<{ id?: string }>();
    const [formId] = useState(params.id ?? generateFormId());

    if (!params.id) {
        saveForm({ id: formId, name: "", fields: [] });
        return <Redirect to={`/designer/${formId}`} />;
    }

    return (
        <Page headerText="Form designer">
            <FormEditor saveForm={saveForm} form={forms[params.id]} />
        </Page>
    );
};
