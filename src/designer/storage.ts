export interface FormConfiguration {
    id: string;
    name: string;
    fields: Field[];
}

export interface Field {
    label: string;
    type: "text" | "email";
}

export const saveForms = (form: FormConfigurationCollection) =>
    window.localStorage.setItem(`forms`, JSON.stringify(form));

export interface FormConfigurationCollection {
    [key: string]: FormConfiguration;
}

export const loadForms = () =>
    JSON.parse(window.localStorage.getItem(`forms`) ?? "[]") as FormConfigurationCollection;
