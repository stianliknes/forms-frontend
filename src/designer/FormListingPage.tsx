import { Page } from "../common/Page";
import { Button, List, ListElementButton, Stack } from "@dossier/mithra-ui";
import { Link } from "react-router-dom";
import React from "react";
import { FormConfigurationCollection } from "./storage";

export const FormListingPage = ({ forms }: { forms: FormConfigurationCollection }) => (
    <Page headerText="Forms">
        <Stack flexDirection="column" alignItems="flex-start">
            <List style={{ width: "100%" }}>
                {Object.keys(forms)
                    .map((key) => forms[key])
                    .map((form) => (
                        <ListElementButton key={form.id} as={Link} to={`/designer/${form.id}`} text={form.name} />
                    ))}
            </List>
            <Button as={Link} to="/designer">
                Create form
            </Button>
        </Stack>
    </Page>
);
