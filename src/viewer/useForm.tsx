import { useLocation } from "react-router-dom";
import { RefObject, useCallback, useEffect, useState } from "react";
import { Field } from "../designer/storage";

export const useForm = (formRef: RefObject<HTMLFormElement>) => {
    const { search } = useLocation();
    const searchParams = new URLSearchParams(search);

    const [fields, setFields] = useState(parseFields(searchParams));
    const [isValid, setFormIsValid] = useState(false);

    const name = searchParams.get("name") ?? "Generic form";

    const updateField = (fieldId: number, value: string) =>
        setFields((prev) => prev.map((v, i) => (i === fieldId ? { ...v, value: value } : v)));

    const reportValidationErrors = useCallback(() => formRef.current?.reportValidity(), [formRef, fields]);

    useEffect(() => {
        setFormIsValid(formRef.current?.checkValidity() ?? false);
    }, [fields, formRef, setFormIsValid]);

    return {
        fields,
        name,
        updateField,
        isValid,
        reportValidationErrors,
    };
};

function parseFields(searchParams: URLSearchParams) {
    return (JSON.parse(searchParams.get("fields") ?? "[]") as Field[]).map((field) => ({ ...field, value: "" }));
}
