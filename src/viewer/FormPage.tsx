import React, { useCallback, useEffect, useRef, useState } from "react";
import { Page } from "../common/Page";
import { Button, FormLabel, Input, Stack } from "@dossier/mithra-ui";
import { useForm } from "./useForm";

const FieldEditor = ({
    label,
    value,
    type,
    id,
    updateField,
}: {
    label: string;
    value?: string;
    type: string;
    id: number;
    updateField: (id: number, value: string) => void;
}) => {
    return (
        <div>
            <FormLabel label={label} />
            <Input type={type} value={value} onChange={(event) => updateField(id, event.target.value)} required />
        </div>
    );
};

export const FormPage: React.FC = () => {
    const formRef = useRef<HTMLFormElement>(null);
    const { name, fields, updateField, reportValidationErrors, isValid } = useForm(formRef);

    return (
        <Page headerText={name}>
            <form ref={formRef}>
                <Stack flexDirection="column" alignItems="flex-start">
                    {fields.map((field, i) => (
                        <FieldEditor key={i} id={i} updateField={updateField} {...field} />
                    ))}
                    {!isValid && <Button onClick={reportValidationErrors}>Save to JSON</Button>}
                    {isValid && (
                        <Button
                            as="a"
                            href={`data:application/json;charset:utf-8,${encodeURIComponent(
                                JSON.stringify(fields, null, 4)
                            )}`}
                            download={name}
                        >
                            Save to JSON
                        </Button>
                    )}
                </Stack>
            </form>
        </Page>
    );
};
